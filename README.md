# <img src="doc/dom_logo.png" width=100> PALM data-output module

The new data-ouput module for PALM (PALM-dom) is designed to create and write output files for PALM.

Its original intention was to generalize the data-output handling of PALM to allow easier implementation of new output.

## How to use

The repository comes with a small testing setup.
To test PALM-dom, you simply have to run the bash script `testing` while inside the main directory:
```bash
./testing build  #compile the sources
./testing test   #run the test program
./testing clean  #delete builds and output
```
Also a parallel version using MPI is available:
```bash
./testing build_mpi  #compile the sources
./testing test_mpi   #run the test program
```
By adding `debug` to the above lines, the program is compiled using debug option for the compiler.
Example:
```bash
./testing test debug
```

Note, when executing `./testing test`, it is first checked if changes were made to the source code and a build is executed if necessary.

Paths to NetCDF libraries and MPI libraries can be adusted in `makefile` and `makefile_mpi`.
Results are saved within the directory `result/`.
Reference files are stored in `results/reference/`.

## Structure

The following graphic shows the planned implementation of PALM-dom into PALM itself (find the ongoing development in the branch 'palm' of this project):

<img src="doc/dom_structure.png">

## Contribution

Please feel free to add comments/ideas/whishes/etc by opening an issue at the project page: <https://gitlab.com/gronemeier/dataout_module>
